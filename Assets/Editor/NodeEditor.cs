﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Pathfinder))]
public class NodeEditor : Editor {
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Pathfinder pf = (Pathfinder)this.target;

        if (GUILayout.Button("Name Nodes"))
        {
            pf.NameNodes();
        }

        if (GUILayout.Button("Find Adjacents"))
        {
            pf.FindAdjacents();
        }
    }
}
