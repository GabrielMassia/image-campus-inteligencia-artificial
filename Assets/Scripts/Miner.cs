﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum States{
    OnPathToMine,
    OnPathToCamp,
    Mining,
    Depositing,
    Idle
};

public class Miner : MonoBehaviour {

    public States state;
    public float oreCapacity;
    public float depositSpeed;
    public float miningSpeed;
    public float moveSpeed;
    public Node camp;
    public Node mine;
    public Pathfinder pathFinder;
    public CullPath pathCuller;
    private float ore;
    private Stack<Vector3> path;
    private Queue<Vector3> finalPath;
    private Vector3 currentTarget;

    private void Update()
    {
        switch (state)
        {
            case States.Depositing:
                CheckOreDelivered();
                break;
            case States.Mining:
                CheckOreCapacity();
                break;
            case States.OnPathToCamp:
                CheckCampProximity();
                break;
            case States.OnPathToMine:
                CheckMineProximity();
                break;
            case States.Idle:
                DoSomethingYouSlacker();
                break;
        }
    }

    private void CheckOreDelivered()
    {
        if (ore > 0)
        {
            Debug.Log("Delivering ore to camp, " + ore.ToString("F1") + " left.\n");
            ore -= Time.deltaTime * depositSpeed;
        }
        else
        {
            Debug.Log("Ore delivered, going back to mine.\n");
            ore = 0;
            state = States.OnPathToMine;
        }
    }

    private void CheckOreCapacity()
    {
        if (ore < oreCapacity)
        {
            Debug.Log("Mining ore, mined " + ore.ToString("F1") + " so far.\n");
            ore += Time.deltaTime * miningSpeed;
        }
        else
        {
            Debug.Log("Reached maximim capacity, going back to camp.\n");
            ore = oreCapacity;
            path = pathFinder.FindPath(ref mine, ref camp);
            finalPath = pathCuller.GetPath(path);
            currentTarget = finalPath.Dequeue();
            state = States.OnPathToCamp;
        }
    }

    private void CheckCampProximity()
    {
        if (Vector3.Distance(transform.position, camp.transform.position) < 0.1f)
        {
            Debug.Log("Reached camp, started ore delivery.\n");
            state = States.Depositing;
        }
        else
        {
            FollowPath();
        }
    }

    private void CheckMineProximity()
    {
        if (Vector3.Distance(transform.position, mine.transform.position) < 0.1f)
        {
            Debug.Log("Reached mine, started ore mining.\n");
            state = States.Mining;
        }
        else
        {
            FollowPath();
        }
    }

    private void DoSomethingYouSlacker()
    {
        Debug.Log("Stopped slacking, going to camp.\n");
        List<Node> nodes = pathFinder.GetNodeList();
        Node closestNode = nodes[0];
        float minDistance = Vector3.Distance(transform.position, closestNode.transform.position);
        float dist;
        for (int i = 1; i < nodes.Count; i++)
        {
            dist = Vector3.Distance(transform.position, nodes[i].transform.position);
            if (dist < minDistance)
            {
                closestNode = nodes[i];
                minDistance = dist;
            }
        }
        path = pathFinder.FindPath(ref closestNode, ref camp);
        finalPath = pathCuller.GetPath(path);
        currentTarget = finalPath.Dequeue();
        state = States.OnPathToCamp;
    }

    private void FollowPath()
    {
        if (Vector3.Distance(transform.position, currentTarget) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, currentTarget, moveSpeed * Time.deltaTime);
        }
        else
        {
            if (finalPath.Count > 0)
            {
                currentTarget = finalPath.Dequeue();
                Debug.Log("Reached previous waypoint, heading to " + currentTarget.ToString() + ".\n");
            }
        }
    }
}