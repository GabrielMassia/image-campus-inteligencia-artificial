﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AlgorithmSwitcher
{
    BreadthFirst,
    Dijkstra,
    AStar
};

public class Pathfinder : MonoBehaviour
{
    public AlgorithmSwitcher algorithm;
    public float adjacentMaxDistance;
    Stack<Vector3> path;
    List<Node> DOpen;
    Queue<Node> open;
    Stack<Node> closed;
    bool pathFound = false;
    private Node node;
    private Node goal;

    [SerializeField]
    private List<Node> nodes = new List<Node>();

    private void Awake()
    {
        DOpen = new List<Node>();
        open = new Queue<Node>();
        closed = new Stack<Node>();
        path = new Stack<Vector3>();
    }

    public Stack<Vector3> FindPath(ref Node Start, ref Node Goal)
    {
        
        for (int i = 0; i < nodes.Count; i++)
        {
            nodes[i].ResetGoal();
            nodes[i].SetGoal(Goal);
        }
        Goal.isGoal = true;
        ResetPath();

        open.Enqueue(Start);
        DOpen.Add(Start);
        node = Start;

        Debug.Log("Start at: " + Start.ToString() + ", goal at: " + Goal.ToString() + ".\n");

        while (!pathFound)
        {
            if (algorithm == AlgorithmSwitcher.BreadthFirst && open.Count > 0)
            {
                if (VisitNode(ref node))
                {
                    GeneratePath(ref node);
                    return path;
                }
                CloseNode(ref node);
                MarkAdjacents(ref node);
            }
            else if ((algorithm == AlgorithmSwitcher.Dijkstra || algorithm == AlgorithmSwitcher.AStar) && DOpen.Count > 0)
            {
                if (VisitNode(ref node))
                {
                    GeneratePath(ref node);
                    return path;
                }
                CloseNode(ref node);
                MarkAdjacents(ref node);
            }
        }

        return path;
    }

    public void ResetPath()
    {
        DOpen.Clear();
        closed.Clear();
        path.Clear();
        pathFound = false;
        for (int i = 0; i < nodes.Count; i++)
        {
            nodes[i].MarkOnHold();
        }
    }

    private bool VisitNode(ref Node node)
    {
        if (algorithm == AlgorithmSwitcher.BreadthFirst)
            node = open.Dequeue();
        else if (algorithm == AlgorithmSwitcher.Dijkstra || algorithm == AlgorithmSwitcher.AStar)
        {
            Node chosenNode = DOpen[0];
            for (int i = 0; i < DOpen.Count; i++)
            {
                if (DOpen[i].weight < chosenNode.weight)
                {
                    chosenNode = DOpen[i];
                }
            }
            node = chosenNode;
            DOpen.Remove(node);
        }
        if (node.isGoal)
        {
            goal = node;
            pathFound = true;
            return true;
        }
        else return false;
    }

    private void GeneratePath(ref Node node)
    {
        while (node != null)
        {
            node.MarkAsPath();
            path.Push(node.gameObject.transform.position);
            node = node.GetParent();
        }
    }

    private void CloseNode(ref Node node)
    {
        node.Close();
        closed.Push(node);
    }

    private void MarkAdjacents(ref Node node)
    {
        for (int i = 0; i < node.adjacent.Count; i++)
        {
            if (node.adjacent[i].GetStatus() == NodeStatus.OnHold)
            {
                node.adjacent[i].Open();
                node.adjacent[i].SetParent(node);
                if (algorithm == AlgorithmSwitcher.BreadthFirst)
                    open.Enqueue(node.adjacent[i]);
                else if (algorithm == AlgorithmSwitcher.Dijkstra || algorithm == AlgorithmSwitcher.AStar)
                {
                    node.adjacent[i].weight += node.score;
                    DOpen.Add(node.adjacent[i]);
                }
            }
        }
    }

    public Stack<Vector3> GetPath()
    {
        return path;
    }

    public void NameNodes()
    {
        GameObject[] goNodes = GameObject.FindGameObjectsWithTag("Node");
        int count = 1;
        goNodes[0].name = count.ToString();

        for (int i = 1; i < goNodes.Length; i++)
        {
            count++;
            goNodes[i].name = count.ToString();
        }
    }

    public void FindAdjacents()
    {
        GameObject[] goNodes = GameObject.FindGameObjectsWithTag("Node");
        Node current;
        Node target;
        
        for (int i = 0; i < goNodes.Length; i++)
        {
            current = goNodes[i].GetComponent<Node>();
            nodes.Add(current);
            for (int j = 0; j < goNodes.Length; j++)
            {
                target = goNodes[j].GetComponent<Node>();
                if (goNodes[i] != goNodes[j] && Vector3.Distance(goNodes[i].transform.position, goNodes[j].transform.position) <= adjacentMaxDistance)
                {
                    current.SetAdjacent(target);
                }
            }
        }

        Debug.Log("Completed. Number of nodes: " + nodes.Count.ToString() + ".\n");
    }

    public List<Node> GetNodeList()
    {
        return nodes;
    }
}