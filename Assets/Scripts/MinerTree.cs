﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourNodes;

public class MinerTree : MonoBehaviour {

    public float oreCapacity;
    public float depositSpeed;
    public float miningSpeed;
    public float moveSpeed;
    public Node camp;
    public Node mine;
    public Pathfinder pathFinder;
    public CullPath pathCuller;
    private float ore;
    private Stack<Vector3> path;
    private Queue<Vector3> finalPath;
    private Vector3 currentTarget;
    private bool hasTarget = false;
    private Blackboard blackboard = new Blackboard();

    void Start () {
        Sequencer main = new Sequencer();
        blackboard.AddNode(main, "Main");

        Selector gotoMine = new Selector();
        blackboard.AddNode(gotoMine, "GotoMine", main);

        Action goMine = new Action(GoMine);
        Action mine = new Action(Mine);
        blackboard.AddNode(goMine, "GoMine", gotoMine);
        blackboard.AddNode(mine, "Mine", gotoMine);

        Selector gotoHome = new Selector();
        blackboard.AddNode(gotoHome, "GotoHome", main);

        Action goHome = new Action(ReturnToBase);
        Action deposit = new Action(DepositOre);
        blackboard.AddNode(goHome, "GoHome", gotoHome);
        blackboard.AddNode(deposit, "Deposit", gotoHome);
    }
	
	void Update () {
        blackboard.UpdateTree();
	}

    private BehaviourNodes.NodeStatus GoMine()
    {
        if(!hasTarget)
        {
            path = pathFinder.FindPath(ref camp, ref mine);
            finalPath = pathCuller.GetPath(path);
            currentTarget = finalPath.Dequeue();
            hasTarget = true;
        }

        if (Vector3.Distance(transform.position, mine.transform.position) < 0.1f)
        {
            Debug.Log("Reached mine, started ore mining.\n");
            hasTarget = false;
            return BehaviourNodes.NodeStatus.True;
        }
        else
        {
            FollowPath();
        }

        return BehaviourNodes.NodeStatus.Processing;
    }

    private BehaviourNodes.NodeStatus Mine()
    {
        if (ore < oreCapacity)
        {
            Debug.Log("Mining ore, mined " + ore.ToString("F1") + " so far.\n");
            ore += Time.deltaTime * miningSpeed;
            return BehaviourNodes.NodeStatus.Processing;
        }
        else
        {
            Debug.Log("Reached maximim capacity, going back to camp.\n");
            ore = oreCapacity;
            return BehaviourNodes.NodeStatus.True;
        }
    }

    private BehaviourNodes.NodeStatus ReturnToBase()
    {
        if (!hasTarget)
        {
            path = pathFinder.FindPath(ref mine, ref camp);
            finalPath = pathCuller.GetPath(path);
            currentTarget = finalPath.Dequeue();
            hasTarget = true;
        }

        if (Vector3.Distance(transform.position, camp.transform.position) < 0.1f)
        {
            Debug.Log("Reached camp, started ore delivery.\n");
            hasTarget = false;
            return BehaviourNodes.NodeStatus.True;
        }
        else
        {
            FollowPath();
        }

        return BehaviourNodes.NodeStatus.Processing;
    }

    private BehaviourNodes.NodeStatus DepositOre()
    {
        if (ore > 0)
        {
            Debug.Log("Delivering ore to camp, " + ore.ToString("F1") + " left.\n");
            ore -= Time.deltaTime * depositSpeed;
            return BehaviourNodes.NodeStatus.Processing;
        }
        else
        {
            Debug.Log("Ore delivered, going back to mine.\n");
            ore = 0;
            return BehaviourNodes.NodeStatus.True;
        }
    }

    private void FollowPath()
    {
        if (Vector3.Distance(transform.position, currentTarget) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, currentTarget, moveSpeed * Time.deltaTime);
        }
        else
        {
            if (finalPath.Count > 0)
            {
                currentTarget = finalPath.Dequeue();
                Debug.Log("Reached previous waypoint, heading to " + currentTarget.ToString() + ".\n");
            }
        }
    }
}