﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NodeStatus {
    OnHold,
    Open,
    Closed
};

public class Node : MonoBehaviour {
    
    public bool isGoal = false;
    public float score = 1;
    public float weight = 0;
    public float heuristic = 0;
    public List<Node> adjacent = new List<Node>();
    private NodeStatus status = NodeStatus.OnHold;
    private Node parent = null;
    private ColorRef colorRef;
    private TextMesh textMesh;
    private Vector3 goal;
    private Pathfinder pf;
    private float originalScore;

    private void Awake()
    {
        originalScore = score;
        weight = score;
        pf = GameObject.Find("Pathfinder").GetComponent<Pathfinder>();
        colorRef = GameObject.Find("ColorRef").GetComponent<ColorRef>();
        textMesh = transform.GetChild(0).GetComponent<TextMesh>();
    }

    public void SetGoal(Node Goal)
    {
        isGoal = false;
        goal = Goal.transform.position;
    }

    private void Update()
    {
        textMesh.text = weight.ToString("F1");
    }

    public void Open()
    {
        status = NodeStatus.Open;
        GetComponent<Renderer>().material = colorRef.Open;
    }

    public void Close()
    {
        status = NodeStatus.Closed;
        GetComponent<Renderer>().material = colorRef.Closed;
    }

    public void MarkAsPath()
    {
        GetComponent<Renderer>().material = colorRef.Path;
    }

    public float FindHeuristic()
    {
        return Vector3.Distance(goal, transform.position);
    }

    public void MarkOnHold()
    {
        status = NodeStatus.OnHold;
        if (isGoal)
            GetComponent<Renderer>().material = colorRef.Path;
        parent = null;
        score = originalScore;
        if (pf.algorithm == AlgorithmSwitcher.AStar)
            score += FindHeuristic();
        weight = score;
    }

    public void ResetGoal()
    {
        isGoal = false;
    }

    public NodeStatus GetStatus()
    {
        return status;
    }

    public void SetParent(Node newParent)
    {
        parent = newParent;
    }

    public Node GetParent()
    {
        return parent;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void SetAdjacent(Node n)
    {
        adjacent.Add(n);
    }
}