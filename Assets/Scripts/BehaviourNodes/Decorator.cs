﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Magic.BehaviourTrees
{
    public class Decorator : BehaviourNode
    {
        public override NodeStatus Status()
        {
            if (children[0].Status() == NodeStatus.False)
            {
                return NodeStatus.True;
            }
            else if (children[0].Status() == NodeStatus.Processing)
            {
                return NodeStatus.Processing;
            }
            else
            {
                return NodeStatus.False;
            }
        }
    }
}