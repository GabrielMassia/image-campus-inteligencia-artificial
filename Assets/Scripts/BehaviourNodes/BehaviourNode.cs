﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Magic.BehaviourTrees
{
    public delegate bool ConditionParam();
    public delegate NodeStatus ActionParam();
    public delegate void MethodToExecute();

    public enum NodeStatus
    {
        True,
        False,
        Processing
    }

    public enum LogicType
    {
        And,
        Or
    }

    public enum ConditionType
    {
        Equals,
        NotEquals,
        IsGreater,
        IsLower,
        IsGreaterOrEqual,
        IsLowerOrEqual,
    }

    public abstract class BehaviourNode : Object
    {
        protected List<BehaviourNode> children;
        protected BehaviourNode parent = null;
        protected int lockIndex = 0;
        private Blackboard bb = null;

        public BehaviourNode()
        {
            children = new List<BehaviourNode>();
        }

        public void AddChild(BehaviourNode child)
        {
            child.parent = this;
            children.Add(child);
        }

        public bool RemoveChild(BehaviourNode child)
        {
            if (children.Contains(child))
            {
                children.Remove(child);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void HaltTree(BehaviourNode action)
        {
            if (parent)
            {
                parent.HaltTree(action);
            }
            else
            {
                bb.HaltTree(action);
            }
        }

        public void SetBlackboard(Blackboard blackboard)
        {
            bb = blackboard;
        }

        public abstract NodeStatus Status();
    }
}