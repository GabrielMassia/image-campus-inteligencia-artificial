﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Magic.BehaviourTrees
{
    public class Logic : BehaviourNode
    {
        private LogicType type;

        public Logic(LogicType logicType)
        {
            type = logicType;
        }

        public override NodeStatus Status()
        {
            switch (type)
            {
                case LogicType.And:

                    for (int i = 0; i < children.Count; i++)
                    {
                        if (children[i].Status() == NodeStatus.False)
                        {
                            return NodeStatus.False;
                        }
                        else if (children[i].Status() == NodeStatus.Processing)
                        {
                            return NodeStatus.Processing;
                        }
                    }
                    return NodeStatus.True;

                case LogicType.Or:

                    for (int i = 0; i < children.Count; i++)
                    {
                        if (children[i].Status() == NodeStatus.True)
                        {
                            return NodeStatus.True;
                        }
                        else if (children[i].Status() == NodeStatus.Processing)
                        {
                            return NodeStatus.Processing;
                        }
                    }
                    return NodeStatus.False;

                default:
                    return NodeStatus.False;
            }
        }
    }
}