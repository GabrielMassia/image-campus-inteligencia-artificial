﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Magic.BehaviourTrees
{
    public class Tree
    {
        private Dictionary<string, BehaviourNode> nodes = new Dictionary<string, BehaviourNode>();
        private List<BehaviourNode> firstLine = new List<BehaviourNode>();
        private BehaviourNode actionInProgress = null;
        private int lockIndex = 0;

        public void UpdateTree()
        {
            if (actionInProgress)
            {
                NodeStatus status = actionInProgress.Status();
                if(status == NodeStatus.Processing)
                {
                    return;
                }
                else
                {
                    actionInProgress = null;
                }
            }
            
            for(int i = lockIndex; i < firstLine.Count; i++)
            {
                NodeStatus status = firstLine[i].Status();
                if (status == NodeStatus.Processing)
                {
                    lockIndex = i;
                    return;
                }
            }
            lockIndex = 0;
        }

        public void AddNode(BehaviourNode node, string name, BehaviourNode parent = null)
        {
            node.SetBlackboard(this);
            if (parent != null)
            {
                parent.AddChild(node);
                nodes.Add(name, node);
            }
            else
            {
                nodes.Add(name, node);
                firstLine.Add(node);
            }
        }

        public void AddNode(string name, BehaviourNode node, string parentName = null)
        {
            node.SetBlackboard(this);
            if (parentName != null && nodes.ContainsKey(parentName))
            {
                nodes[parentName].AddChild(node);
                nodes.Add(name, node);
            }
            else
            {
                nodes.Add(name, node);
                firstLine.Add(node);
            }

        }

        public bool RemoveNode(string name)
        {
            if (nodes.ContainsKey(name))
            {
                nodes.Remove(name);
                return true;
            }
            return false;
        }

        public bool RemoveNode(BehaviourNode node)
        {
            string key = null;
            foreach(string k in nodes.Keys)
            {
                if (node == nodes[key])
                {
                    key = k;
                    break;
                }
            }

            if (key != null)
            {
                RemoveNode(key);
                return true;
            }
            return false;
        }

        public void HaltTree(BehaviourNode action)
        {
            actionInProgress = action;
        }
    }
}