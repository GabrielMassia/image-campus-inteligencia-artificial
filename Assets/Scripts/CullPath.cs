﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CullPath : MonoBehaviour {

    public GameObject pathMarker;
    private List<GameObject> markers;
    public Pathfinder pf;

    private void Start()
    {
        markers = new List<GameObject>();
    }

    public void SetPathFinder(Pathfinder pathFinder)
    {
        pf = pathFinder;
    }

    public void ClearMarkers()
    {
        for (int i = 0; i < markers.Count; i++)
            Destroy(markers[i]);
        if(markers != null)
            markers.Clear();
    }

    public Queue<Vector3> GetPath(Stack<Vector3> positions)
    {
        ClearMarkers();

        if (positions == null || positions.Count < 2)
            return null;

        List<Vector3> pos = new List<Vector3>();
        while(positions.Count > 0)
        {
            pos.Add(positions.Pop());
        }

        Queue<Vector3> result = new Queue<Vector3>();
        int currentIndex = 1;
        Vector3 currentPos = pos[currentIndex - 1];
        Vector3 currentTarget;
        Vector3 behindTarget;
        bool vis = true;
        bool validFound;

        while(currentPos != pos[pos.Count-1])
        {
            Vector3 validPos = new Vector3();
            validFound = false;

            Debug.Log("Node " + (currentIndex - 1).ToString() + " checking against everything onwards.\n");

            for(int i = currentIndex; i < pos.Count; i++)
            {
                currentTarget = pos[i];
                behindTarget = pos[i - 1];

                if (Physics.Linecast(currentPos, currentTarget) && vis)
                {
                    validPos = behindTarget;
                    currentIndex = i;
                    validFound = true;
                    vis = false;

                    Debug.Log("Hit detected at node " + i.ToString() + ", valid node at " + (currentIndex - 1).ToString() + ". Disabling visibility.\n");
                }
                else if (!Physics.Linecast(currentPos, currentTarget) && !vis)
                {
                    vis = true;

                    Debug.Log("Hit not detected at node " + i.ToString() + ". Resetting visibility.\n");
                }
                else if (Physics.Linecast(currentPos, currentTarget) && !vis)
                {
                    Debug.Log("Hit not registered at node " + i.ToString() + ", visibility is not enabled.\n");
                }
                else
                {
                    Debug.Log("Hit not detected at node " + i.ToString() + ".\n");
                }
            }

            Debug.Log("Current node finished checking.\n");

            if (validFound)
            {
                result.Enqueue(validPos);

                Debug.Log("Enqueueing valid node.\n\n");
            }
            else
            {
                currentPos = pos[pos.Count - 1];

                Debug.Log("Obstacles not found from the next node onwards. Skipping to end\n\n.");

                break;
            }
            currentPos = pos[currentIndex - 1];
        }
        result.Enqueue(currentPos);

        Vector3 p;
        for(int i = 0; i < result.Count; i++)
        {
            p = result.Dequeue();
            markers.Add(Instantiate(pathMarker, p, Quaternion.identity));
            result.Enqueue(p);
        }

        return result;
    }
}