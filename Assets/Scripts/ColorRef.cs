﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorRef : MonoBehaviour {

    public Material OnHold;
    public Material Closed;
    public Material Open;
    public Material Path;
}
